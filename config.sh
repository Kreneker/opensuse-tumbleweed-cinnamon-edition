#!/bin/bash
#================
# FILE          : config.sh
#----------------
# PROJECT       : OpenSuSE KIWI Image System
# COPYRIGHT     : (c) 2006 SUSE LINUX Products GmbH. All rights reserved
#               :
# AUTHOR        : Marcus Schaefer <ms@suse.de>
#               :
# BELONGS TO    : Operating System images
#               :
# DESCRIPTION   : configuration script for SUSE based
#               : operating systems
#               :
#               :
# STATUS        : BETA
#----------------
#======================================
# Functions...
#--------------------------------------
test -f /.kconfig && . /.kconfig
test -f /.profile && . /.profile

#======================================
# Greeting...
#--------------------------------------
echo "Configure image: [$kiwi_iname]..."

#======================================
# Mount system filesystems
#--------------------------------------
baseMount

#======================================
# Setup baseproduct link
#--------------------------------------
suseSetupProduct

#======================================
# Add missing gpg keys to rpm
#--------------------------------------
suseImportBuildKey
rpm --import /keys/cinnamon_factory.key
rpm --import /keys/cinnamon_andy.key
rpm --import /keys/opensuse-oss.key
#======================================
# Disable services
#--------------------------------------
baseRemoveService wicked
baseRemoveService wickedd
baseRemoveService wickedd-auto4
baseRemoveService wickedd-dhcp4
baseRemoveService wickedd-dhcp5
baseRemoveService wickedd-nanny
#======================================
# Activate services
#--------------------------------------
suseInsertService sshd
baseInsertService apparmor
baseInsertService appstream-sync-cache
baseInsertService auditd
baseInsertService bluetooth
baseInsertService cron
baseInsertService display-manager
baseInsertService firewalld
baseInsertService getty@tty1
baseInsertService haveged
baseInsertService irqbalance
baseInsertService iscsi
baseInsertService mcelog
baseInsertService ModemManager
baseInsertService NetworkManager
baseInsertService NetworkManager-dispatcher
baseInsertService NetworkManager-wait-online
baseInsertService nscd
baseInsertService purge-kernels
baseInsertService smartd
baseInsertService systemd-remount-fs
baseInsertService systemd-timesyncd

#======================================
# Setup default target, multi-user
#--------------------------------------

#==========================================
# remove package docs
#------------------------------------------
rm -rf /usr/share/doc/packages/*
rm -rf /usr/share/doc/manual/*
rm -rf /opt/kde*

#==========================================
# configure and install flatpak apps
#------------------------------------------


#======================================
# only basic version of vim is
# installed; no syntax highlighting
#--------------------------------------
sed -i -e's/^syntax on/" syntax on/' /etc/vimrc

#======================================
# SuSEconfig
#--------------------------------------
baseUpdateSysConfig /etc/sysconfig/windowmanager DEFAULT_WM muffin
baseUpdateSysConfig /etc/sysconfig/displaymanager DISPLAYMANAGER lightdm
baseUpdateSysConfig /etc/sysconfig/displaymanager DISPLAYMANAGER_AUTOLOGIN tux
baseSetRunlevel 5
suseConfig

#======================================
# Remove yast if not in use
#--------------------------------------


#======================================
# Umount kernel filesystems
#--------------------------------------
baseCleanMount

exit 0
